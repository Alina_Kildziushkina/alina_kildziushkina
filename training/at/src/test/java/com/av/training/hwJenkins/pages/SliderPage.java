package com.av.training.hwJenkins.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SliderPage extends Page {

    private final By sliderItem = By.xpath("//div[@id='slider']/span");

    public WebElement getSliderItem() {
        return driver.findElement(sliderItem);
    }
}

