package com.av.training.hwJenkins.tests;

import com.av.training.hwJenkins.pages.CheckboxradioPage;
import com.av.training.hwJenkins.pages.MainPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckboxradioTest extends BaseTest {
    @Test
    public void checkboxradioTest() {
        CheckboxradioPage checkboxradioPage = new MainPage().clickCheckboxradioLink();
        checkboxradioPage.switchToFrame();
        checkboxradioPage.chooseNewYorkRadioButton()
                .choosetwoStarsCheckBox();
        Assert.assertTrue(checkboxradioPage.getNewYorkRadioButton().getAttribute("class").contains("checked"));
        Assert.assertTrue(checkboxradioPage.getTwoStarsCheckBox().getAttribute("class").contains("checked"));
    }
}
