package com.av.training.hwJenkins.tests;

import com.av.training.hwJenkins.pages.DroppablePage;
import com.av.training.hwJenkins.pages.MainPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DroppableTest extends BaseTest {
    @Test
    public void dragAndDropTest() {
        DroppablePage droppablePage = new MainPage().clickDroppableLink();
        droppablePage.switchToFrame();
        new Actions(driver).dragAndDrop(droppablePage.getDraggableElement(), droppablePage.getDroppableElement())
                .build()
                .perform();
        Assert.assertTrue(droppablePage.getDroppableElement().getAttribute("class").contains("highlight"));
    }
}
