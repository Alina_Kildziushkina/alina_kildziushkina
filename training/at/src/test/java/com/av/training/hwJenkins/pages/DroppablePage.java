package com.av.training.hwJenkins.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DroppablePage extends Page {

    private final By draggableElement = By.xpath("//div[@id='draggable']");
    private final By droppableElement = By.xpath("//div[@id='droppable']");

    public WebElement getDraggableElement() {
        return driver.findElement(draggableElement);
    }

    public WebElement getDroppableElement() {
        return driver.findElement(droppableElement);
    }
}
