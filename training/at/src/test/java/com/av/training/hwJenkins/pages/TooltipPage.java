package com.av.training.hwJenkins.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TooltipPage extends Page {

    private final By tooltip = By.xpath("//a[@href='#']");

    public WebElement getTooltip() {
        return driver.findElement(tooltip);
    }
}
