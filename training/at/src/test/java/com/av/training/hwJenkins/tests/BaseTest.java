package com.av.training.hwJenkins.tests;

import com.av.training.hwJenkins.browser.Browser;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {
    protected WebDriver driver;
    private static final String TEST_URL = "http://jqueryui.com";

    @BeforeClass
    public void setUp() {
        driver = Browser.getWebDriverInstance();
        driver.get(TEST_URL);
    }

    @AfterClass
    public void reset() {
        Browser.closeDriver();
    }
}
