package com.av.training.hwJenkins.pages;

import com.av.training.hwJenkins.browser.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
    protected final WebDriver driver = Browser.getWebDriverInstance();
    private final By frame = By.className("demo-frame");

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected boolean elementIsPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    public void switchToFrame() {
        driver.switchTo().frame(driver.findElement(frame));
    }

    public void switchToDefault() {
        driver.switchTo().defaultContent();
    }


}
