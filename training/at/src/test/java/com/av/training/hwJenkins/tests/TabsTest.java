package com.av.training.hwJenkins.tests;

import com.av.training.hwJenkins.pages.MainPage;
import com.av.training.hwJenkins.pages.TabsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TabsTest extends BaseTest {

    @Test
    public void switchTabsTest() {
        TabsPage tabsPage = new MainPage().clickTabsLink();
        tabsPage.switchToFrame();
        Assert.assertTrue(tabsPage.getFirstTab().getAttribute("tabindex").contains("0"));
        tabsPage.switchTab(tabsPage.getThirdTab());
        Assert.assertTrue(tabsPage.getFirstTab().getAttribute("tabindex").contains("-1"));
        Assert.assertTrue(tabsPage.getThirdTab().getAttribute("tabindex").contains("0"));
    }
}
