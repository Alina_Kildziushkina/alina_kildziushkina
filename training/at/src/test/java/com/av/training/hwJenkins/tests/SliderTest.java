package com.av.training.hwJenkins.tests;

import com.av.training.hwJenkins.pages.MainPage;
import com.av.training.hwJenkins.pages.SliderPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SliderTest extends BaseTest {

    @Test
    public void slederTest() {
        SliderPage sliderPage = new MainPage().clickSliderLink();
        sliderPage.switchToFrame();
        Actions actions = new Actions(driver);
        actions.dragAndDropBy(sliderPage.getSliderItem(), 120, 120).build().perform();
        Assert.assertTrue(!sliderPage.getSliderItem().getAttribute("style").contains("left: 0%;"));
    }
}
