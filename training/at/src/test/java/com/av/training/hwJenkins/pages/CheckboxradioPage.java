package com.av.training.hwJenkins.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CheckboxradioPage extends Page {

    private final By newYorkRadioButton = By.xpath("//label[@for='radio-1']");
    private final By twoStarsCheckBox = By.xpath("//label[@for='checkbox-1']");

    public WebElement getNewYorkRadioButton() {
        return driver.findElement(newYorkRadioButton);
    }

    public WebElement getTwoStarsCheckBox() {
        return driver.findElement(twoStarsCheckBox);
    }

    public CheckboxradioPage chooseNewYorkRadioButton() {
        driver.findElement(newYorkRadioButton).click();
        return this;
    }

    public CheckboxradioPage choosetwoStarsCheckBox() {
        driver.findElement(twoStarsCheckBox).click();
        return this;
    }
}
