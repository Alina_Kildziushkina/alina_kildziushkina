package com.av.training.hwJenkins.tests;

import com.av.training.hwJenkins.pages.MainPage;
import com.av.training.hwJenkins.pages.TooltipPage;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class TooltipTest extends BaseTest {

    @Test
    public void tooltipTest() {
        TooltipPage tooltipPage = new MainPage().clickToolTipLink();
        tooltipPage.switchToFrame();
        new Actions(driver).moveToElement(tooltipPage.getTooltip());
        org.junit.Assert.assertTrue(tooltipPage.getTooltip().getAttribute("title").startsWith("That's what this widget is"));
    }
}
