package com.av.training.hwJenkins.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TabsPage extends Page {

    private final By firstTab = By.xpath("//li[@aria-controls='tabs-1']");
    private final By secondTab = By.xpath("//li[@aria-controls='tabs-2']");
    private final By thirdTab = By.xpath("//li[@aria-controls='tabs-3']");

    public WebElement getFirstTab() {
        return driver.findElement(firstTab);
    }

    public WebElement getSecondTab() {
        return driver.findElement(secondTab);
    }

    public WebElement getThirdTab() {
        return driver.findElement(thirdTab);
    }

    public TabsPage switchTab(WebElement tab) {
        tab.click();
        return this;
    }
}
