package com.av.training.hwJenkins.pages;

import org.openqa.selenium.By;

public class MainPage extends Page {

    private final By droppableLink = By.xpath("//div[@id='sidebar']//a[contains(@href, 'droppable')]");
    private final By checkboxradioLink = By.xpath("//div[@id='sidebar']//a[contains(@href, 'checkboxradio')]");
    private final By selectmenuLink = By.xpath("//div[@id='sidebar']//a[contains(@href, 'selectmenu')]");
    private final By tooltipLink = By.xpath("//div[@id='sidebar']//a[contains(@href, 'tooltip')]");
    private final By sliderLink = By.xpath("//div[@id='sidebar']//a[contains(@href, 'slider')]");
    private final By tabsLink = By.xpath("//div[@id='sidebar']//a[contains(@href, 'tabs')]");

    public DroppablePage clickDroppableLink() {
        driver.findElement(droppableLink).click();
        return new DroppablePage();
    }

    public CheckboxradioPage clickCheckboxradioLink() {
        driver.findElement(checkboxradioLink).click();
        return new CheckboxradioPage();
    }

    public SelectmenuPage clickSelectMenuLink() {
        driver.findElement(selectmenuLink).click();
        return new SelectmenuPage();
    }

    public TooltipPage clickToolTipLink() {
        driver.findElement(tooltipLink).click();
        return new TooltipPage();
    }

    public SliderPage clickSliderLink() {
        driver.findElement(sliderLink).click();
        return new SliderPage();
    }

    public TabsPage clickTabsLink() {
        driver.findElement(tabsLink).click();
        return new TabsPage();
    }
}
