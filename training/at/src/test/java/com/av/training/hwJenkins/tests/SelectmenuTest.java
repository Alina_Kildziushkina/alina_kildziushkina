package com.av.training.hwJenkins.tests;

import com.av.training.hwJenkins.pages.MainPage;
import com.av.training.hwJenkins.pages.SelectmenuPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SelectmenuTest extends BaseTest {
    @Test
    public void selectMenuTest() {
        SelectmenuPage selectmenuPage = new MainPage().clickSelectMenuLink();
        selectmenuPage.switchToFrame();
        selectmenuPage.openSpeedDropDown()
                .chooseFastSpeedDropDownItem()
                .openNumberDropDown()
                .chooseThreeNumberDropDownItem();
        Assert.assertTrue(selectmenuPage.getSpeedDropDown().getAttribute("aria-activedescendant").startsWith("ui-id-4"));
        Assert.assertTrue(selectmenuPage.getNumberDropDown().getAttribute("aria-activedescendant").startsWith("ui-id-8"));
    }
}
