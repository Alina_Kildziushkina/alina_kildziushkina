package com.av.training.hwJenkins.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SelectmenuPage extends Page {

    private final By speedDropDown = By.id("speed-button");
    private final By numberDropDown = By.id("number-button");
    private final By fastSpeedDropDownItem = By.id("ui-id-4");
    private final By threeNumberDropDownItem = By.id("ui-id-8");

    public SelectmenuPage openSpeedDropDown() {
        driver.findElement(speedDropDown).click();
        return this;
    }

    public SelectmenuPage openNumberDropDown() {
        driver.findElement(numberDropDown).click();
        return this;
    }

    public SelectmenuPage chooseFastSpeedDropDownItem() {
        driver.findElement(fastSpeedDropDownItem).click();
        return this;
    }

    public SelectmenuPage chooseThreeNumberDropDownItem() {
        driver.findElement(threeNumberDropDownItem).click();
        return this;
    }

    public WebElement getSpeedDropDown() {
        return driver.findElement(speedDropDown);
    }

    public WebElement getNumberDropDown() {
        return driver.findElement(numberDropDown);
    }
}
