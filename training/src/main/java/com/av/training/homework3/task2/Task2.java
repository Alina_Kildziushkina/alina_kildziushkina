package com.av.training.homework3.task2;

import java.util.Arrays;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("Median count for int type with odd number of elements " + Median.median(new int[]{1, 8, 3, 0, 7}));
        System.out.println("Median count for int type with even number of elements " + Median.median(new int[]{9, 4, 1, 8, 5, 2}));
        System.out.println("Median count for double type with odd number of elements " + Median.median(new double[]{1.5, 8.3, 3.7, 0.2, 7.56}));
        System.out.println("Median count for double type with even number of elements " + Median.median(new double[]{9.9, 4.9, 1.1, 8.7, 5.6, 2.6}));
    }
}

class Median {

    public static float median(int[] array) {
        Arrays.sort(array);
        if ((array.length - 1) % 2 == 0)
            return array[(array.length - 1) / 2];
        else
            return (float) (array[array.length / 2] + array[(array.length / 2) - 1]) / 2;
    }

    public static double median(double[] array) {
        Arrays.sort(array);
        if ((array.length - 1) % 2 == 0)
            return array[(array.length - 1) / 2];
        else
            return (array[array.length / 2] + array[(array.length / 2) - 1]) / 2;
    }
}