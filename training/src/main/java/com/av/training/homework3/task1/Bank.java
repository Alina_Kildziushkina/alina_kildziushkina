package com.av.training.homework3.task1;

public class Bank {
    public static void main(String[] args) {
        Card card1 = new Card("Alina", 100.25);
        System.out.println("Card balance is " + card1.getCardBalance());
        card1.topUpCardBalance(45.68);
        System.out.println("Card balance is " + card1.getCardBalance());
        card1.reduceCardBalance(15.13);
        System.out.println("Card balance is " + card1.getCardBalance());
        System.out.format("Card balance after conversion is %.2f%n", card1.convertCardBalance(0.536));
        System.out.println("__________________________________________________");
        Card card2 = new Card("Kate");
        System.out.println("Card balance is " + card2.getCardBalance());
        card2.topUpCardBalance(152.63);
        System.out.println("Card balance is " + card2.getCardBalance());
        card2.reduceCardBalance(65.13);
        System.out.println("Card balance is " + card2.getCardBalance());
        System.out.format("Card balance after conversion is %.2f%n", card2.convertCardBalance(2.1104));
    }
}

class Card {
    private String ownerName;
    private double cardBalance;

    Card(String ownerName, double startBalance) {
        this.ownerName = ownerName;
        this.cardBalance = startBalance;
    }

    Card(String ownerName) {
        this(ownerName, 0.0);
    }

    public double getCardBalance() {
        return this.cardBalance;
    }

    public double topUpCardBalance(double replenishment) {
        this.cardBalance += replenishment;
        return this.cardBalance;
    }

    public double reduceCardBalance(double reduction) {
        this.cardBalance -= reduction;
        return this.cardBalance;
    }

    public double convertCardBalance(double rate) {
        double convertedCardBalance = this.cardBalance * rate;
        return convertedCardBalance;
    }
}
