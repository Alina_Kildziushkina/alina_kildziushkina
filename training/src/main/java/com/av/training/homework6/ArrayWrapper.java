package com.av.training.homework6;

public class ArrayWrapper<T> {

    private T[] array;

    public ArrayWrapper(T[] array) {
        this.array = array;
    }

    public T get(int index) {
        if (index > 0 && index <= array.length)
            return array[index - 1];
        else
            throw new IncorrectArrayWrapperIndex("The index is out of range!");
    }

    public boolean replace(int index, T change) {
        if (index > 0 && index <= array.length) {
            if (array instanceof String[]) {
                if (array[index - 1].toString().length() < change.toString().length()) {
                    array[index - 1] = change;
                    return true;
                }
                return false;
            } else if (array instanceof Integer[]) {
                if ((Integer) array[index - 1] < (Integer) change) {
                    array[index - 1] = change;
                    return true;
                }
                return false;
            } else {
                array[index - 1] = change;
                return true;
            }
        } else {
            throw new IncorrectArrayWrapperIndex("The index is out of range!");
        }
    }
}
