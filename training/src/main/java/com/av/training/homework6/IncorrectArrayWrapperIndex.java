package com.av.training.homework6;

public class IncorrectArrayWrapperIndex extends RuntimeException {

    public IncorrectArrayWrapperIndex(String message) {
        super(message);
    }
}
