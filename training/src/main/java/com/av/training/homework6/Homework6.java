package com.av.training.homework6;

public class Homework6 {
    public static void main(String[] args) {
        ArrayWrapper<String> arrayWrapperString = new ArrayWrapper<>(new String[]{"first", "second", "third", "forth"});
        ArrayWrapper<Integer> arrayWrapperInteger = new ArrayWrapper<>(new Integer[]{5, 4, 3, 2, 1});
        System.out.println("Element 3 of String array: " + arrayWrapperString.get(3));
        System.out.println("Element 2 of Integer array: " + arrayWrapperInteger.get(2));
        arrayWrapperString.replace(3, "replaced element");
        System.out.println("Element 3 of String array after replacement: " + arrayWrapperString.get(3));
        arrayWrapperInteger.replace(2, 195);
        System.out.println("Element 2 of Integer array after replacement: " + arrayWrapperInteger.get(2));
    }
}
