package com.av.training.homework7.task1;

import java.util.Arrays;
import java.util.Comparator;

public class SortArrayClass {
    public static void main(String[] args) {
        int[] intArray = new int[args.length];
        String output = "Array before sorting: ";
        for (int i = 0; i < args.length; i++) {
            intArray[i] = Integer.parseInt(args[i]);
            output += intArray[i] + " ";
        }
        System.out.println(output);
        System.out.println("Sorting the array in the order of the sum of digits of its elements ...");
        int[] sorted = Arrays.stream(intArray)
                .boxed()
                .sorted(Comparator.comparing(i -> sum(i)))
                .mapToInt(i -> i)
                .toArray();
        System.out.println("Array after sorting: " + Arrays.toString(sorted));
    }

    public static int sum(int num) {
        return Integer.toString(num).chars().map(c -> c -= '0').sum();
    }
}
