package com.av.training.homework7.task2;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MathOperations {
    public static void main(String[] args) {
        boolean cycle = true;
        while (cycle) {
            try {
                Scanner in = new Scanner(System.in);

                System.out.print("Input the number of elements in the first set: ");
                int numberOfElements1 = in.nextInt();
                HashSet<Integer> hashSet1 = new HashSet<>(numberOfElements1);
                System.out.println("Input the first set: ");

                for (int i = 0; i < numberOfElements1; i++) {
                    System.out.print("set[" + i + "]= ");
                    hashSet1.add(in.nextInt());
                }

                System.out.print("Input the number of elements in the second set: ");
                int numberOfElements2 = in.nextInt();
                HashSet<Integer> hashSet2 = new HashSet<>(numberOfElements2);
                System.out.println("Input the second set: ");
                for (int i = 0; i < numberOfElements2; i++) {
                    System.out.print("set[" + i + "]= ");
                    hashSet2.add(in.nextInt());
                }
                System.out.println("First set: " + hashSet1);
                System.out.println("Second set: " + hashSet2);
                in.close();

                System.out.println("Union of sets: " + findUnionOdSets(hashSet1, hashSet2));

                System.out.println("Intersection of sets: " + findIntersectionOfSets(hashSet1, hashSet2));

                System.out.println("Difference of sets (hashSet1 - hashSet2): " + findDifferenceOfSets(hashSet1, hashSet2));

                System.out.println("Difference of sets (hashSet2 - hashSet1): " + findDifferenceOfSets(hashSet2, hashSet1));

                System.out.println("Inclusion exclusion principle: " + countInclusionExclusionPrinciple(hashSet1, hashSet2));
                cycle = false;
            } catch (InputMismatchException ex) {
                System.out.println("The input is incorrect!!! Enter the number with integer type!!!!!");
                continue;
            }
        }
    }

    public static HashSet findUnionOdSets(HashSet hashSet1, HashSet hashSet2) {
        HashSet<Integer> transformedHashSet = new HashSet<>();
        transformedHashSet.addAll(hashSet1);
        transformedHashSet.addAll(hashSet2);
        return transformedHashSet;
    }

    public static HashSet findIntersectionOfSets(HashSet hashSet1, HashSet hashSet2) {
        HashSet<Integer> transformedHashSet = new HashSet<>();
        transformedHashSet.addAll(hashSet1);
        transformedHashSet.retainAll(hashSet2);
        return transformedHashSet;
    }

    public static HashSet findDifferenceOfSets(HashSet hashSet1, HashSet hashSet2) {
        HashSet<Integer> transformedHashSet = new HashSet<>();
        transformedHashSet.addAll(hashSet1);
        transformedHashSet.removeAll(hashSet2);
        return transformedHashSet;
    }

    public static HashSet countInclusionExclusionPrinciple(HashSet hashSet1, HashSet hashSet2) {
        HashSet<Integer> transformedHashSet = findUnionOdSets(hashSet1, hashSet2);
        transformedHashSet.removeAll(findIntersectionOfSets(hashSet1, hashSet2));
        return transformedHashSet;
    }
}
