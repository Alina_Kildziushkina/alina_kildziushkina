package com.av.training.homework2.task3;

public class Recursion {
    public static void main(String[] args) {
        int i = 1;
        System.out.println("Counting Fibonachi for Integer ...");
        int intVar = fibonachiRecForInt(i);
        while (intVar <= Integer.MAX_VALUE && intVar > 0) {
            System.out.println(intVar + " (" + i + ")");
            i++;
            intVar = fibonachiRecForInt(i);
        }
        System.out.println((i - 1) + " numbers of Fibonachi is in positive Integer.");
        System.out.println("Counting Fibonachi for Long ...");
        i = 1;
        long longVar = fibonachiRecForLong(i);
        while (longVar <= Long.MAX_VALUE && longVar > 0) {
            System.out.println(longVar + " (" + i + ")");
            i++;
            longVar = fibonachiRecForLong(i);
        }
        System.out.println((i - 1) + " numbers of Fibonachi is in positive Long.");
    }

    public static int fibonachiRecForInt(int n) {
        if (n == 1 || n == 2)
            return 1;
        else
            return fibonachiRecForInt(n - 1) + fibonachiRecForInt(n - 2);
    }

    public static long fibonachiRecForLong(long l) {
        if (l == 1 || l == 2)
            return 1;
        else
            return fibonachiRecForLong(l - 1) + fibonachiRecForLong(l - 2);
    }
}
