package com.av.training.homework2.task2;

public class OrderOfPriority {

    //It is static variable. It loads when the class (OrderOfPriority) is loaded in the Java machine.
    static String name = writeText("static");
    //It is ordinary variable. It loads when the class object (orderOfPriority) is loaded.
    String surname = writeText("ordinary");

    public static void main(String[] args) {
        Person person1; //Creation of a reference variable in the main method
        System.out.println("\n");
        person1 = new Student();  //Object reference assignment
        System.out.println("\n");
        Person person2 = new Person();  //Object reference assignment
        OrderOfPriority orderOfPriority = new OrderOfPriority();  //Class instance creation
    }

    public static String writeText(String s) {
        System.out.println("This is " + s + " variable!");
        return "";
    }
}

class Person {
    Person() {
        System.out.println("Constructor class Person");  //Constructor of a Person class
    }

    {
        System.out.println("Initialization block class Person");   //Initialization block (class Person)
    }

    static {
        System.out.println("Static initialization block class Person");  //Static initialization block (class Person)
    }
}

class Student extends Person {
    Student() {
        System.out.println("Constructor class Student");   //Constructor of a Student class
    }

    {
        System.out.println("Initialization block class Student");   //Initialization block (class Student)
    }

    static {
        System.out.println("Static initialization block class Student");  //Static initialization block (class Student)
    }
}
