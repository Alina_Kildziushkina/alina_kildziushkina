package com.av.training.homework2.task1;

public class Initializer {
    static int i;
    static float f;
    static long l;
    static double d;
    static short s;
    static byte b;
    static char c;
    static boolean bool;
    static String someString;

    public static void main(String[] args) {
        System.out.println("Default values of primitive data types:");
        System.out.println("Boolean: " + bool);
        System.out.println("Byte: " + b);
        System.out.println("Short: " + s);
        System.out.println("Int: " + i);
        System.out.println("Long: " + l);
        System.out.println("Float: " + f);
        System.out.println("Double: " + d);
        System.out.println("Char: " + c + " //Default value of char is not a printable character \u0000");
        System.out.println("\nDefault values of reference data types:");
        System.out.println("String: " + someString);
        SomeClass someClass = new SomeClass();
        System.out.println("Class: " + someClass);
    }
}

class SomeClass {
}

