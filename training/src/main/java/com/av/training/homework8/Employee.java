package com.av.training.homework8;

import java.io.Serializable;

class Employee implements Serializable {
    private int id;
    private String firstName;
    private String lastName;

    public int getId() {
        return id;
    }

    public Employee(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String toString() {
        return "\nEmployee [Id = " + this.id
                + ", first name = " + this.firstName
                + ", last name = " + this.lastName + "]";
    }
}
