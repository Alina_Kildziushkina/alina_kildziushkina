package com.av.training.homework8;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class EmployeeTableEditor {
    static Scanner in = new Scanner(System.in);
    static List<Employee> employeeList = new ArrayList<>();

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        System.out.println("_______________Menu_________________");
        System.out.println("1. Show the table with all employees");
        System.out.println("2. Take employees list from the table");
        System.out.println("3. Add a new employee");
        System.out.println("4. Remove an employee");
        System.out.println("5. Save the changes");
        System.out.println("6. Exit");
        System.out.println("------------------------------------");
        System.out.print("Choose one of the menu item: ");
        int menuItem;
        try {
            menuItem = in.nextInt();
            switch (menuItem) {
                case 1:
                    showListOfEmployees();
                    break;
                case 2:
                    readFromFile();
                    break;
                case 3:
                    addNewEmployee();
                    break;
                case 4:
                    removeEmployee();
                    break;
                case 5:
                    saveChanges("menu");
                    break;
                case 6:
                    exitProgram();
                    break;
                default:
                    System.out.println("Check the input!!!!");
                    menu();
                    break;
            }
        } catch (InputMismatchException ex) {
            System.out.println("Enter the number from the list!!!!!");
        }
    }

    public static void checkListIsEmpty() {
        if (employeeList.isEmpty()) {
            System.out.println("There are no employees in the list!");
            menu();
        }
    }

    public static void showListOfEmployees() {
        checkListIsEmpty();
        System.out.println(employeeList.toString() + "\n");
        menu();
    }

    public static void readFromFile() {
        if (!employeeList.isEmpty()) {
            System.out.println("Do you want to save the changes? Y/N");
            String answer = in.next();
            if (answer.equals("Y") || answer.equals("y") || answer.equals("yes") || answer.equals("Yes")) {
                saveChanges("readFromFile");
            }
        }
        List<Employee> readEmployeeList = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream("employeeList.emp");
            ObjectInputStream ois = new ObjectInputStream(fis);
            readEmployeeList = (List<Employee>) ois.readObject();
            ois.close();
        } catch (Exception e) {
        }
        employeeList.clear();
        employeeList.addAll(readEmployeeList);
        showListOfEmployees();
    }

    public static void addNewEmployee() {
        System.out.println("Addition of employee ...");
        System.out.println("In order to go back to menu enter: MENU");
        System.out.print("Enter first name: ");
        String firstName = in.next();
        if (firstName.equals("MENU") || firstName.equals("menu") || firstName.equals("Menu"))
            menu();
        else {
            System.out.print("Enter last name: ");
            String lastName = in.next();
            employeeList.add(new Employee(employeeList.size() + 1, firstName, lastName));
            System.out.println("Employee is added!");
            menu();
        }
    }

    public static void removeEmployee() {
        checkListIsEmpty();
        int numberOfEmployees = employeeList.size();
        System.out.println("Removing employee ...");
        System.out.println("In order to go back to menu enter: MENU");
        System.out.println("In order to remove all employees enter: all");
        System.out.print("Enter ID: ");
        String idToRemove = in.next();
        if (idToRemove.equals("MENU") || idToRemove.equals("menu") || idToRemove.equals("Menu")) {
            menu();
        } else if (idToRemove.equals("all")) {
            employeeList.clear();
        }
        for (Employee employee : employeeList) {
            if (Integer.parseInt(idToRemove) == employee.getId()) {
                employeeList.remove(employee);
                System.out.println("Employee is removed!!!");
                menu();
            }
        }
        if (numberOfEmployees == employeeList.size()) {
            System.out.println("Incorrect input!!!");
            removeEmployee();
        }
    }

    public static void saveChanges(String method) {
        try {
            FileOutputStream fs = new FileOutputStream("employeeList.emp");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(employeeList);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Changes are saved!!");
        if (method.equals("menu"))
            menu();
        else if (method.equals("readFromFile")) {
            employeeList.clear();
            readFromFile();
        } else if (method.equals("exitProgram"))
            System.exit(0);
    }

    public static void exitProgram() {
        System.out.println("Do you want to save changes? (Y/N)");
        String answer = in.next();
        if (answer.equals("Y") || answer.equals("y") || answer.equals("yes") || answer.equals("Yes")) {
            saveChanges("exitProgram");
        } else if (answer.equals("N") || answer.equals("n") || answer.equals("no") || answer.equals("No"))
            System.exit(0);
        else {
            System.out.println("Enter correct answer!!");
            exitProgram();
        }
    }
}
