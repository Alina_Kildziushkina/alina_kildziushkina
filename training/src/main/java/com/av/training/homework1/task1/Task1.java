package com.av.training.homework1.task1;

public class Task1 {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);
        double m1 = Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);
        if (m1 + m2 == 0 || p == 0)
            System.out.println("Check the entered data: m1+m2 or p is 0");
        else {
            double g = 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2));
            System.out.println("The result is: G=" + g);
        }
    }
}
