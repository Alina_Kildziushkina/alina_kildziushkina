package com.av.training.homework1.task2;

public class Task2 {
    public static void main(String[] args) {
        int algorithmId = Integer.parseInt(args[0]);
        int loopType = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);
        switch (algorithmId) {
            case 1: {
                switch (loopType) {
                    case 1:
                        countFibonachiWhile(n);
                        break;
                    case 2:
                        countFibonachiDoWhile(n);
                        break;
                    case 3:
                        countFibonachiFor(n);
                        break;
                    default:
                        System.out.println("The second input is incorrect");
                }
            }
            break;
            case 2: {
                switch (loopType) {
                    case 1:
                        countFactorialWhile(n);
                        break;
                    case 2:
                        countFactorialDoWhile(n);
                        break;
                    case 3:
                        countFactorialFor(n);
                        break;
                    default:
                        System.out.println("The second input is incorrect");
                }
            }
            break;
            default:
                System.out.println("The first input is incorrect");
        }
    }

    public static void countFibonachiWhile(int n) {
        System.out.println("Fibonachi counted with While: ");
        int f1 = 1;
        int f2 = 1;
        if (n == 1)
            System.out.println(f1);
        else if (n == 2)
            System.out.println(f1 + "\n" + f2);
        else {
            System.out.println(f1 + "\n" + f2);
            int i = 0;
            int sum;
            while (i < (n - 2)) {
                sum = f1 + f2;
                f1 = f2;
                f2 = sum;
                System.out.println(sum);
                i++;
            }
        }
    }

    public static void countFibonachiDoWhile(int n) {
        System.out.println("Fibonachi counted with Do While: ");
        int f1 = 1;
        int f2 = 1;
        if (n == 1)
            System.out.println(f1);
        else if (n == 2)
            System.out.println(f1 + "\n" + f2);
        else {
            System.out.println(f1 + "\n" + f2);
            int i = 0;
            int sum;
            do {
                sum = f1 + f2;
                f1 = f2;
                f2 = sum;
                System.out.println(sum);
                i++;
            }
            while (i < (n - 2));
        }
    }

    public static void countFibonachiFor(int n) {
        System.out.println("Fibonachi counted with For: ");
        int f1 = 1;
        int f2 = 1;
        if (n == 1)
            System.out.println(f1);
        else if (n == 2)
            System.out.println(f1 + "\n" + f2);
        else {
            System.out.println(f1 + "\n" + f2);
            int sum;
            for (int i = 0; i < (n - 2); i++) {
                sum = f1 + f2;
                f1 = f2;
                f2 = sum;
                System.out.println(sum);
            }
        }
    }

    public static void countFactorialWhile(int n) {
        int factorial = 1;
        int i = 2;
        while (i <= n) {
            factorial *= i;
            i++;
        }
        System.out.println("Factorial " + n + " counted with For: " + factorial);
    }

    public static void countFactorialDoWhile(int n) {
        int factorial = 1;
        int i = 1;
        do {
            factorial *= i;
            i++;
        }
        while (i <= n);
        System.out.println("Factorial " + n + " counted with For: " + factorial);
    }

    public static void countFactorialFor(int n) {
        int factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        System.out.println("Factorial " + n + " counted with For: " + factorial);
    }
}
