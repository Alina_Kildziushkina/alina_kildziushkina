package com.av.training.homework9;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class EmployeeDataBaseEditor {
    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/db_for_m4_l3.db");
            menu(connection);
            connection.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void removeEmployeeById(Connection connection) {
        try {
            System.out.println("Enter the employee id for removing: ");
            int id = in.nextInt();
            Statement statement = connection.createStatement();
            statement.executeUpdate(String.format("delete from Employee where id = %s", id));
            System.out.println("The employee with " + id + " was deleted");

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        menu(connection);
    }

    public static void showAllEmployees(Connection connection) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from Employee");
            while (resultSet.next()) {
                String id = resultSet.getString("id");
                String firstname = resultSet.getString(2);
                String lastname = resultSet.getString(3);

                System.out.println(id + " " + firstname + " " + lastname);
            }

            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        menu(connection);
    }

    public static void addEmployee(Connection connection) {
        try {
            System.out.println("Enter new employee first name: ");
            String firstname = in.next();
            System.out.println("Enter new employee last name: ");
            String lastname = in.next();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select max(id) from Employee ");
            Employee employee = new Employee(rs.getInt(1) + 1, firstname, lastname);
            statement.executeUpdate(String.format("insert into Employee(firstname, lastname) values('%s', '%s')", employee.getFirstName(), employee.getLastName()));
            System.out.println("The employee " + employee.getFirstName() + " " + employee.getLastName() + " was added");
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        menu(connection);
    }

    public static void menu(Connection connection) {
        System.out.println("_______________Menu_________________");
        System.out.println("1. Show all employees");
        System.out.println("2. Add a new employee");
        System.out.println("3. Remove an employee");
        System.out.println("4. Exit");
        System.out.println("------------------------------------");
        System.out.print("Choose one of the menu item: ");
        int menuItem;
        try {
            menuItem = in.nextInt();
            switch (menuItem) {
                case 1:
                    showAllEmployees(connection);
                    break;
                case 2:
                    addEmployee(connection);
                    break;
                case 3:
                    removeEmployeeById(connection);
                    break;
                case 4:
                    exitProgram(connection);
                    break;
                default:
                    System.out.println("Check the input!!!!");
                    menu(connection);
                    break;
            }
        } catch (InputMismatchException ex) {
            System.out.println("Enter the number from the list!!!!!");
        }
    }

    public static void exitProgram(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
