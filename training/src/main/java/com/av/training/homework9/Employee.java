package com.av.training.homework9;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String toString() {
        return "\nEmployee [Id = " + this.id
                + ", first name = " + this.firstName
                + ", last name = " + this.lastName + "]";
    }
}
