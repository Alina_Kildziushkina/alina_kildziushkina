package com.av.training.homework4.task2;

class SortingContext {
    private Sorter sorter;

    public SortingContext(Sorter sorter) {
        this.sorter = sorter;
    }

    public int[] executeSorter(int[] array) {
        return sorter.sort(array);
    }
}
