package com.av.training.homework4.task2;

class SelectionSort extends Sorter {
    int[] sort(int[] array) {
        for (int i = 0; i < array.length; i++)
            for (int j = (i + 1); j < array.length; j++)
                if (array[i] > array[j]) {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }

        return array;
    }
}
