package com.av.training.homework4.task1;

public class Task41 {
    public static void main(String[] args) {
        Atm atm1 = new Atm("credit", "Alina", 250);
        atm1.getCardBalance();
        atm1.reduceCardBalance(260);
        atm1.getCardBalance();
        atm1.topUpCardBalance(530);
        atm1.getCardBalance();
        atm1.convertCardBalance(2.356);
        System.out.println("______________________________________________");
        Atm atm2 = new Atm("debit", "Peter", 420);
        atm2.getCardBalance();
        atm2.reduceCardBalance(460);
        atm2.getCardBalance();
        atm2.reduceCardBalance(160);
        atm2.getCardBalance();
        atm2.topUpCardBalance(340);
        atm2.getCardBalance();
        atm2.convertCardBalance(2.555);
    }
}
