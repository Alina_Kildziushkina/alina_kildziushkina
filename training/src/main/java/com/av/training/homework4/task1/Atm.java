package com.av.training.homework4.task1;

class Atm {

    Card card;

    Atm(String cardType, String name, double balance) {
        if (cardType == "credit")
            card = new CreditCard(name, balance);
        else if (cardType == "debit")
            card = new DebitCard(name, balance);
        else
            System.out.println("There is no such a type of card!");
    }

    public String getCardBalance() {
        return card.getCardBalance();
    }

    public double topUpCardBalance(double replenishment) {
        return card.topUpCardBalance(replenishment);
    }

    public double reduceCardBalance(double reduction) {
        return card.reduceCardBalance(reduction);
    }

    public String convertCardBalance(double rate) {
        return card.convertCardBalance(rate);
    }
}
