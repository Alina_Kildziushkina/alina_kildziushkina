package com.av.training.homework4.task1;

import java.text.DecimalFormat;

class Card {
    private String ownerName;
    protected double cardBalance;
    private DecimalFormat decimalFormat = new DecimalFormat("###.##");
    private String formatCardBalance;

    Card(String ownerName, double startBalance) {
        this.ownerName = ownerName;
        this.cardBalance = startBalance;
    }

    Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getCardBalance() {
        formatCardBalance = decimalFormat.format(this.cardBalance);
        System.out.println("Card balance is " + formatCardBalance);
        return formatCardBalance;
    }

    public double topUpCardBalance(double replenishment) {
        this.cardBalance += replenishment;
        return this.cardBalance;
    }

    public double reduceCardBalance(double reduction) {
        this.cardBalance -= reduction;
        return this.cardBalance;
    }

    public String convertCardBalance(double rate) {
        double convertedCardBalance = this.cardBalance * rate;
        formatCardBalance = decimalFormat.format(convertedCardBalance);
        System.out.println("Card balance with the conversion rate " + rate + " is " + formatCardBalance);
        return formatCardBalance;
    }
}
