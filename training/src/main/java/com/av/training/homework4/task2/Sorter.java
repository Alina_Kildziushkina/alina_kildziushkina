package com.av.training.homework4.task2;

abstract class Sorter {
    abstract int[] sort(int[] array);
}
