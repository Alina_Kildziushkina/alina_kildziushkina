package com.av.training.homework4.task1;

class CreditCard extends Card {
    CreditCard(String ownerName, double startBalance) {
        super(ownerName, startBalance);
    }

    @Override
    public double reduceCardBalance(double reduction) {
        if (super.cardBalance < reduction)
            System.out.println("You have accumulated the debt in the amount of " + (reduction - super.cardBalance));
        return super.reduceCardBalance(reduction);
    }
}
