package com.av.training.homework4.task2;

public class Strategy {
    public static void main(String[] args) {
        int[] array = new int[]{2, 4, 8, 3, 5, 9};
        SortingContext sortingContext = new SortingContext(new SelectionSort());
        array = sortingContext.executeSorter(array);

        System.out.println("Selection sort : ");
        for (int i = 0; i < array.length; i++)
            System.out.println(array[i]);

        sortingContext = new SortingContext(new BubbleSort());
        array = sortingContext.executeSorter(array);

        System.out.println("Bubble sort : ");
        for (int i = 0; i < array.length; i++)
            System.out.println(array[i]);
    }
}
