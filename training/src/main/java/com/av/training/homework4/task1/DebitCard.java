package com.av.training.homework4.task1;

class DebitCard extends Card {
    DebitCard(String ownerName, double startBalance) {
        super(ownerName, startBalance);
    }

    @Override
    public double reduceCardBalance(double reduction) {
        if (super.cardBalance < reduction) {
            System.out.println("Your balance is lower than the sum of reduction. You can't withdraw this sum.");
            return super.cardBalance;
        } else
            return super.reduceCardBalance(reduction);
    }
}
