package com.av.training.homework5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;

public class TextProcessor {
    public static void main(String[] args) throws IOException {
        String fileName = "src/main/java/com/av/training/homework5/text.txt";
        String contents = readUsingBufferedReader(fileName);
        contents = convertToLowerCase(contents);
        contents = deleteCommas(contents);
        displayTextByWordsAlphabetically(contents);
    }

    private static String readUsingBufferedReader(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        String line = "";
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    private static String convertToLowerCase(String contents) {
        return contents.toLowerCase();
    }

    private static String deleteCommas(String contents) {
        contents = contents.replaceAll("\\p{Punct}", " ");
        return contents;
    }

    private static void displayTextByWordsAlphabetically(String contents) {
        SortedSet<String> lineSet = new TreeSet<String>();
        String[] str = contents.split("\\s");
        for (int i = 0; i < str.length; i++) {
            lineSet.add(str[i]);
        }
        for (char i = 'A'; i <= 'Z'; i++) {
            String s = String.valueOf(i);
            String output = s + ": ";
            int numberWordsForLetter = 0;
            for (String line1 : lineSet) {
                if (line1.startsWith(s.toLowerCase())) {
                    numberWordsForLetter++;
                    output += line1 + " ";
                }
            }
            if (numberWordsForLetter > 0)
                System.out.println(output);
        }
    }
}
