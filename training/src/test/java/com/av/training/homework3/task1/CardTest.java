package com.av.training.homework3.task1;

import org.junit.Before;

import static org.junit.Assert.assertEquals;

public class CardTest {

    public Card card;
    public Card card2;

    @Before
    public void setUp() {
        card = new Card("Alina", 100.55);
        card2 = new Card("Peter");
    }

    @org.junit.Test
    public void getCardBalance() {
        double expected = 100.55;
        assertEquals(expected, card.getCardBalance(), 0.01);
    }

    @org.junit.Test
    public void getCardBalanceWithoutStartBalance() {
        double expected = 0;
        assertEquals(expected, card2.getCardBalance(), 0.01);
    }

    @org.junit.Test
    public void topUpCardBalance() {
        double expected = 151.01;
        assertEquals(expected, card.topUpCardBalance(50.46), 0.01);
    }

    @org.junit.Test
    public void reduceCardBalance() {
        double actual = card.reduceCardBalance(50.44);
        double expected = 50.11;
        assertEquals(expected, actual, 0.01);
    }

    @org.junit.Test
    public void convertCardBalance() {
        double actual = card.convertCardBalance(1.5861);
        double expected = 159.48;
        assertEquals(expected, actual, 0.01);
    }
}