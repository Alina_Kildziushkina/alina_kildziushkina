package com.av.training.homework4.task1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AtmTest {

    public Atm atmCredit;
    public Atm atmDebit;

    @Before
    public void setUp() {
        atmCredit = new Atm("credit", "Masha", 1200);
        atmDebit = new Atm("debit", "Roman", 560);
    }

    @Test
    public void getCardBalanceCredit() {
        double actual = Double.parseDouble(atmCredit.getCardBalance());
        assertEquals(1200, actual, 0.01);
    }

    @Test
    public void getCardBalanceDebit() {
        double actual = Double.parseDouble(atmDebit.getCardBalance());
        assertEquals(560, actual, 0.01);
    }

    @Test
    public void topUpCardBalanceCredit() {
        double actual = atmCredit.topUpCardBalance(580);
        assertEquals(1780, actual, 0.01);
    }

    @Test
    public void topUpCardBalanceDebit() {
        double actual = atmDebit.topUpCardBalance(40.55);
        assertEquals(600.55, actual, 0.01);
    }

    @Test
    public void reduceCardBalanceCreditWithoutDebt() {
        double actual = atmCredit.reduceCardBalance(1099.16);
        assertEquals(100.84, actual, 0.01);
    }

    @Test
    public void reduceCardBalanceCreditWithDebt() {
        double actual = atmCredit.reduceCardBalance(1300.41);
        assertEquals(-100.41, actual, 0.01);
    }

    @Test
    public void reduceCardBalanceDebitWithoutDebt() {
        double actual = atmDebit.reduceCardBalance(500.86);
        assertEquals(59.14, actual, 0.01);
    }

    @Test
    public void reduceCardBalanceDebitWithDebt() {
        double actual = atmDebit.reduceCardBalance(590.1);
        assertEquals(560, actual, 0.01);
    }

    @Test
    public void convertCardBalanceCredit() {
        String actual = atmCredit.convertCardBalance(2.56371);
        assertEquals("3076,45", actual);
    }

    @Test
    public void convertCardBalanceDebit() {
        String actual = atmDebit.convertCardBalance(1.8623);
        assertEquals("1042,89", actual);
    }
}